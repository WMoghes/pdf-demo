<?php
ini_set("display_errors", 1);

function dd($var)
{
    print_r('<pre>');print_r($var); print_r('</pre>');die();
}
function dump($var)
{
    print_r('<pre>');print_r($var); print_r('</pre>');
}

function addFirstPage($pdf, $sourceFile)
{
    // import a page
    $templateId = $pdf->importPage(1);
    // get the size of the imported page
    $size = $pdf->getTemplateSize($templateId);

    // add a page with the same orientation and size
    $pdf->AddPage($size['orientation'], $size);

    // use the imported page
    $pdf->useTemplate($templateId);
}

function addLastPage($pdf, $sourceFile)
{
    // import a page
    $templateId = $pdf->importPage($sourceFile);
    // get the size of the imported page
    $size = $pdf->getTemplateSize($templateId);

    // add a page with the same orientation and size
    $pdf->AddPage($size['orientation'], $size);

    // use the imported page
    $pdf->useTemplate($templateId);
}

use setasign\Fpdi\Fpdi;

require 'vendor/autoload.php';

$pdf = new Fpdi();

addFirstPage($pdf, $pdf->setSourceFile('files/coverfooter.pdf'));

$pageCount = $pdf->setSourceFile('files/142271305.pdf');

for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
    // import a page
    $templateId = $pdf->importPage($pageNo);

    $pdf->AddPage();
    // use the imported page and adjust the page size
    $pdf->useTemplate($templateId, ['adjustPageSize' => true]);
}

addLastPage($pdf, $pdf->setSourceFile('files/coverfooter.pdf'));

//echo '<h1>Done</h1>';

$pdf->Output();